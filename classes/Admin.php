<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 4/16/2019
 * Time: 12:37 PM
 */

namespace Admin;


use PDO;

class Admin
{
    public $id;
    public $username;
    public $password;
    private $_db;

    /**
     * Calls the Database when User class is called.
     *
     * @param $conn
     */
    public function __construct($conn)
    {
        $this->_db = $conn;
    }

    /**
     * Initiate the Login Process
     * @return bool     Returns either true or false
     */
    public function login()
    {
        $query = "SELECT id
                  FROM admin
                  WHERE 
                  username = :username";

        // Prepare the Sql Query
        $stmt = $this->_db->prepare($query);

        // Bind Statement
        $stmt->bindParam(":username", $this->username);

        // Execute Query
        $stmt->execute();

        // Rows Returned
        $rows = $stmt->rowCount();

        if ($rows > 0) {
            $adminInfo = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->id = $adminInfo['id'];

            return true;
        } else {
            return false;
        }
    }
}
