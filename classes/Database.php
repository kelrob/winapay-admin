<?php
/**
 * Created by WINAPAY LIMITED
 * Author: ROBERT EBAFUA
 * Date: 2/3/2019
 * Time: 12:48 PM
 * Database Class
 *
 * @copyright  WINAPAY LIMITED
 * @license    Private to WINAPAY LIMITED
 * @link       https://winapay.com
 * @since      Class available since Release 2.0
 */

namespace Database;


use PDO;
use PDOException;

class Database
{
    # Database Properties
    private $_host = "localhost";
    private $_dbName = "winapayc_mywinapay";
    private $_username = "winapayc_live_db";
    private $_password = "?]U*kw5RNN=t";
    public $conn;


    /**
     * This Gets the database connection.
     *
     * @return string   This returns the Connection
     */
    public function getConnection()
    {
        $this->conn = null;

        try {
            $this->conn = new PDO("mysql:host=" . $this->_host . ";dbname=" . $this->_dbName, $this->_username, $this->_password);
        } catch (PDOException $exception) {
            echo "Connection error: " . $exception->getMessage();
        }

        return $this->conn;
    }

}