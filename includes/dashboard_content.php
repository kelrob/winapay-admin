<?php
include_once 'functions/general.php';
$wquery = mysqli_query($connection, "SELECT * FROM `plans_expiry`");
$rows_returned = mysqli_num_rows($wquery);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>WinApay | Dashboard</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
        <!-- Fontastic Custom icon font-->
        <link rel="stylesheet" href="css/fontastic.css">
        <!-- Google fonts - Poppins -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="css/custom.css">
        <!-- Favicon-->
        <link rel="shortcut icon" href="img/favicon.png">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        <?php
            include 'includes/upload_verification.php';
            $temp = mysqli_query($connection, "SELECT * FROM `temp`");
            if (isset($_POST['upd'])) {
                $e = sanitize($_POST['e']);
                $h = sanitize($_POST['h']);
                
                if (($e + $h) == 10) {
                    $q = mysqli_query($connection, "UPDATE `temp` SET `E`='$e', `H`='$h' WHERE `id`='1'");
                    if ($q) {
                        $message = 'Updated Sucessfully';
                    }
                } else {
                    $message = 'Total needs to be up to 100%';
                }
            }
        ?>
    </head>
<body>
    <div class="page">
        <!-- Main Navbar-->
        <header class="header">
            <nav class="navbar">

                <!-- Search Box-->
                <div class="search-box">
                    <button class="dismiss"><i class="icon-close"></i></button>
                    <form id="searchForm" action="#" role="search">
                        <input type="search" placeholder="What are you looking for..." class="form-control">
                    </form>
                </div>

                <div class="container-fluid">
                    <div class="navbar-holder d-flex align-items-center justify-content-between">

                        <!-- Navbar Header-->
                        <div class="navbar-header">
                            <!-- Navbar Brand -->
                            <a href="index.html" class="navbar-brand">
                                <div class="brand-text brand-big">
                                    <span>WinApay </span><strong>Dashboard</strong>
                                </div>
                                <div class="brand-text brand-small">
                                    <strong>W</strong>
                                </div>
                            </a>
                            <!-- Toggle Button-->
                            <a id="toggle-btn" href="#" class="menu-btn active">
                                <span></span><span></span><span></span>
                            </a>
                        </div>

                        <!-- Navbar Menu -->
                        <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">

                            <!-- Search-->
                            <li class="nav-item d-flex align-items-center">
                                <a id="search" href="#">
                                    <i class="icon-search"></i>
                                </a>
                            </li>

                            <!-- Notifications-->
                            <li class="nav-item dropdown"> 
                                <a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link">
                                    <i class="fa fa-bell-o"></i>
                                    <span class="badge bg-red"><?php echo getInstantPlayWinners(); ?></span>
                                </a>
                                <ul aria-labelledby="notifications" class="dropdown-menu">
                                    
                                   
                                    
                                   
                                    
                                        <a rel="nofollow" href="notifications.php" class="dropdown-item all-notifications text-center"> 
                                            <strong>view all notifications</strong>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <!-- Messages-->
                            <li class="nav-item dropdown"> 
                                <a id="messages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="badge bg-orange"><?php displayNoOfUnreadMessages(); ?></span>
                                </a>
                                
                                
                                <ul aria-labelledby="notifications" class="dropdown-menu">


                                      <li>
                                        <a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                            <div class="msg-profile"> 
                                                <img src="img/avatar-1.jpg" alt="..." class="img-fluid rounded-circle">
                                            </div>
                                            <div class="msg-body">
                                              <h3 class="h5"><?php $sender_email ?></h3><span><?php echo $sender_email .' just paid in'; ?></span>
                                            </div>
                                        </a>
                                    </li>  

                                    <li>
                                        <a rel="nofollow" href="messages.php" class="dropdown-item all-notifications text-center"> 
                                            <strong>Read all messages</strong>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <!-- Logout    -->
                            <li class="nav-item">
                                <a href="login.html" class="nav-link logout">Logout<i class="fa fa-sign-out"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <div class="page-content d-flex align-items-stretch"> 
            <!-- Side Navbar -->
            <nav class="side-navbar">
                <!-- Sidebar Header-->
                <div class="sidebar-header d-flex align-items-center">
                    <div class="avatar">
                        <img src="img/avatar-1.jpg" alt="..." class="img-fluid rounded-circle">
                    </div>
                    <div class="title">
                        <h1 class="h4"><?php echo $adminUsername; ?></h1>
                        <p>Admin</p>
                    </div>
                </div>
                <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
                <ul class="list-unstyled">
                    <li class="active"><a href="dashboard.php"> <i class="icon-home"></i>Home </a></li>
                    <li><a href="cash.php"> <i class="fa fa-money"></i> Disburse Cash</a></li>
                    <li><a href="request.php"> <i class="fa fa-money"></i>Withdrawal Request <span class="badge bg-red"><?php echo withdrawal_request(); ?></span> </a></li>
                </ul>
            </nav>
            <div class="content-inner">
                <!-- Page Header-->

                <!-- Feeds Section-->
                <!-- Dashboard Header Section    -->
          <section class="dashboard-header">
            <div class="container-fluid">
              <div class="row">
                <!-- Statistics -->
                <div class="statistics col-lg-3 col-12">
                  <div class="statistic d-flex align-items-center bg-white has-shadow">
                    <div class="icon bg-red"><i class="fa fa-tasks"></i></div>
                    <div class="text">
                        <strong>&#8358;<?php echo totalInstantAttempts() * 200; ?></strong>
                        <br>
                        <small>
                            INSTANT PLAY (<?php echo number_format(totalInstantAttempts()); ?>)
                        </small>
                    </div>
                  </div>
                  <div class="statistic d-flex align-items-center bg-white has-shadow">
                    <div class="icon bg-green"><i class="fa fa-calendar-o"></i></div>
                    <div class="text">
                        <strong>
                            <?php
                                $daily_value = 50 * totalDailyAttempts();
                                echo '&#8358;'.number_format($daily_value);
                            ?>
                        </strong>
                        <br>
                        <small>
                            DAILY PLAN
                            (<?php echo number_format(totalDailyAttempts()); ?>)
                        </small>
                    </div>
                  </div>
                  <div class="statistic d-flex align-items-center bg-white has-shadow">
                    <div class="icon bg-orange"><i class="fa fa-paper-plane-o"></i></div>
                    <div class="text">
                        <strong>
                            <?php 
                                $monthly_value = 1500 * totalMonthlySubscribers();
                                echo '&#8358;'.number_format($monthly_value);
                            ?>
                        </strong>
                        <br>
                        <small>
                            MONTHLY PLAY 
                            (<?php echo number_format(totalMonthlyAttempts()); ?>)
                        </small>
                    </div>
                  </div>
                  <div class="statistic d-flex align-items-center bg-white has-shadow">
                    <div class="icon bg-orange"><i class="fa fa-paper-plane-o"></i></div>
                    <div class="text"><strong>&#8358;0</strong><br><small>EXPENSES (0)</small></div>
                  </div>
                </div>
                <!-- Line Chart            -->
                <div class="chart col-lg-6 col-12">
                  <div class="line-chart bg-white d-flex align-items-center justify-content-center has-shadow">
                    <canvas id="lineCahrt"></canvas>
                  </div>
                </div>
                <div class="chart col-lg-3 col-12">
                  <!-- Bar Chart   -->
                  <div class="bar-chart has-shadow bg-white">
                    <div class="title"><strong class="text-violet">&#8358;<?php echo number_format(cashInBank()); ?></strong><br><small>Cash in Account</small></div>
                    <canvas id="barChartHome"></canvas>
                  </div>
                  <!-- Numbers-->
                  <div class="statistic d-flex align-items-center bg-white has-shadow">
                    <div class="icon bg-blue"><i class="fa fa-line-chart"></i></div>
                    <div class="text"><strong>&#8358;<?php echo number_format(cashOutflow()); ?></strong><br><small>Cash Outflow</small></div>
                  </div>
                  
                  <!-- Numbers-->
                  <div class="statistic d-flex align-items-center bg-white has-shadow">
                    <div class="icon bg-green"><i class="fa fa-line-chart"></i></div>
                    <div class="text"><strong>&#8358;<?php echo number_format(companyRevenue()); ?></strong><br><small>Revenue</small></div>
                  </div>
                </div>
              </div>
            </div>
          </section>


                <footer class="main-footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">
                                <p>WinApay &copy; 2018-2020</p>
                            </div>
                            <div class="col-sm-6 text-right">
                                <p>Design by <a href="https://bootstrapious.com/admin-templates" class="external">Bootstrapious</a></p>
                                <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>
    <!-- Javascript files-->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="vendor/popper.js/umd/popper.min.js"> </script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="js/charts-home.js"></script>
    <!-- Main File-->
    <script src="js/front.js"></script>
</body>
</html>