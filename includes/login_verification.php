<?php

if (isset($_POST['login'])) {

	$adminUsername = sanitize($_POST['loginUsername']);
	$adminPassword = sanitize($_POST['loginPassword']);

	# Check if Admin exists
	if (adminExists($adminUsername, $adminPassword) != false) {

		$loggedAdminId = adminExists($adminUsername, $adminPassword);
		$_SESSION['w_admin_id'] = $loggedAdminId;
		header('Location: dashboard.php');

	} else {
		$error = '<p style="color: red;">Invalid Login Details</p>';
	}

}

?>