<?php
include_once 'functions/general.php';
$wquery = mysqli_query($connection, "SELECT * FROM `bonus`");
$rows_returned = mysqli_num_rows($wquery);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>WinApay | Dashboard</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
        <!-- Fontastic Custom icon font-->
        <link rel="stylesheet" href="css/fontastic.css">
        <!-- Google fonts - Poppins -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="css/custom.css">
        <!-- Favicon-->
        <link rel="shortcut icon" href="img/favicon.png">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        <?php
            include 'includes/upload_verification.php';
        ?>
    </head>
<body>
    <div class="page">
        <!-- Main Navbar-->
        <header class="header">
            <nav class="navbar">

                <!-- Search Box-->
                <div class="search-box">
                    <button class="dismiss"><i class="icon-close"></i></button>
                    <form id="searchForm" action="#" role="search">
                        <input type="search" placeholder="What are you looking for..." class="form-control">
                    </form>
                </div>

                <div class="container-fluid">
                    <div class="navbar-holder d-flex align-items-center justify-content-between">

                        <!-- Navbar Header-->
                        <div class="navbar-header">
                            <!-- Navbar Brand -->
                            <a href="index.html" class="navbar-brand">
                                <div class="brand-text brand-big">
                                    <span>WinApay </span><strong>Dashboard</strong>
                                </div>
                                <div class="brand-text brand-small">
                                    <strong>W</strong>
                                </div>
                            </a>
                            <!-- Toggle Button-->
                            <a id="toggle-btn" href="#" class="menu-btn active">
                                <span></span><span></span><span></span>
                            </a>
                        </div>

                        <!-- Navbar Menu -->
                        <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">

                            <!-- Search-->
                            <li class="nav-item d-flex align-items-center">
                                <a id="search" href="#">
                                    <i class="icon-search"></i>
                                </a>
                            </li>

                            <!-- Notifications-->
                            <li class="nav-item dropdown"> 
                                <a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link">
                                    <i class="fa fa-bell-o"></i>
                                    <span class="badge bg-red"><?php displayNoOfWinners(); ?></span>
                                </a>
                                <ul aria-labelledby="notifications" class="dropdown-menu">
                                    
                                    <?php
                                        $query = mysqli_query($connection, "SELECT * FROM `winners` where `status`='0' ORDER BY `id` DESC LIMIT 3");
                                        
                                        while($details = mysqli_fetch_array($query)) {
                                            $id = $details['id'];
                                            $user_id = $details['user_id'];
                                            $plan =  $details['plan'];
                                            $time = $details['time'];
                                            
                                            
                                            
                                            $fullname = getUserField($user_id, 'fullname');
                                    ?>
                                        
                                       <li>
                                            <a rel="nofollow" href="#" class="dropdown-item"> 
                                                <div class="notification">
                                                    <div class="notification-content">
                                                        <i class="fa fa-money bg-green"></i><?php echo $fullname . ' Just won  from plan ' . $plan;  ?>
                                                    </div>
                                                    <div class="notification-time">
                                                        <small><?php echo date('D M Y', $time); ?></small>
                                                    </div>
                                                </div>
                                            </a>
                                        </li> 
                                        
                                    <?php
                                        }
                                    ?>
                                    
                                   
                                    
                                        <a rel="nofollow" href="notifications.php" class="dropdown-item all-notifications text-center"> 
                                            <strong>view all notifications</strong>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <!-- Messages-->
                            <li class="nav-item dropdown"> 
                                <a id="messages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="badge bg-orange"><?php displayNoOfUnreadMessages(); ?></span>
                                </a>
                                
                                
                                <ul aria-labelledby="notifications" class="dropdown-menu">
                                    
                                    <?php
                                        $query = mysqli_query($connection, "SELECT * FROM `messages` WHERE `is_read` = '0' ORDER BY `id` DESC LIMIT 3");
                                        
                                        while($details = mysqli_fetch_array($query)) {
                                            $sender_id = $details['sender_id'];
                                            $sender_fullname =  getUserField($sender_id, 'fullname');
                                            $message_subject = $details['message_subject'];
                                    ?>
                                        
                                      <li>
                                        <a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                            <div class="msg-profile"> 
                                                <img src="img/avatar-1.jpg" alt="..." class="img-fluid rounded-circle">
                                            </div>
                                            <div class="msg-body">
                                              <h3 class="h5"><?php echo $sender_fullname ?></h3><span><?php echo $message_subject; ?></span>
                                            </div>
                                        </a>
                                    </li>  
                                        
                                    <?php
                                        }
                                    ?>
                                    
                                    
                                    <li>
                                        <a rel="nofollow" href="messages.php" class="dropdown-item all-notifications text-center"> 
                                            <strong>Read all messages</strong>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <!-- Logout    -->
                            <li class="nav-item">
                                <a href="login.html" class="nav-link logout">Logout<i class="fa fa-sign-out"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <div class="page-content d-flex align-items-stretch"> 
            <!-- Side Navbar -->
            <nav class="side-navbar">
                <!-- Sidebar Header-->
                <div class="sidebar-header d-flex align-items-center">
                    <div class="avatar">
                        <img src="img/avatar-1.jpg" alt="..." class="img-fluid rounded-circle">
                    </div>
                    <div class="title">
                        <h1 class="h4"><?php echo $adminUsername; ?></h1>
                        <p>Admin</p>
                    </div>
                </div>
                <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
                <ul class="list-unstyled">
                    <li><a href="dashboard.php"> <i class="icon-home"></i>Home </a></li>
                    <li><a href="home.php"> <i class="icon-home"></i>Ask a Question </a></li>
                    <li><a href="notify.php"> <i class="fa fa-bell-o"></i>Notify User </a></li>
                    <li><a href="cash.php"> <i class="fa fa-money"></i>Add Cash / Deduct </a></li>
                    <li><a href="request.php"> <i class="fa fa-money"></i>Withdrawal Request <span class="badge bg-red"><?php echo withdrawal_request(); ?></span> </a></li>
                    <li><a href="information.php"> <i class="fa fa-money"></i>User Information </a></li>
                    <li><a href="users.php"> <i class="fa fa-money"></i>Users List <span class="badge bg-red"><?php echo users_list(); ?></span>  </a></li>
                    <li><a href="clear.php"> <i class="fa fa-money"></i>Clear Unused Bonus <span class="badge bg-red"><?php echo $rows_returned; ?></span> </a></li>
                    <li class="active"><a href="questions.php"> <i class="fa fa-bookmark"></i>Questions</a></li>
                </ul>
            </nav>
            <div class="content-inner">
                <!-- Page Header-->
                <header class="page-header">
                    <div class="container-fluid">
                        <h2 class="no-margin-bottom">
                            Dashboard | <small>Summary</small>
                        </h2>
                        <br />
                        <?php
                            if (getPromo('switch') == 0) {
                        ?>
                                <p><a href="promo.php?promo=on" class="btn btn-primary">Turn on Promo</a> Promo is off</p>
                        <?php
                            } else {
                        ?>
                                <p><a href="promo.php?promo=off" class="btn btn-danger">Turn off Promo</a> Promo is on</p>
                        <?php
                            }
                        ?>
                        
                    </div>
                </header>

                <!-- Feeds Section-->
                <!-- Dashboard Header Section    -->
          <section class="dashboard-header">
            <div class="container-fluid">
              <div class="row">
                <!-- Statistics -->
                <div class="statistics col-lg-3 col-12">
                  <div class="statistic d-flex align-items-center bg-white has-shadow">
                    <div class="icon bg-red"><i class="fa fa-globe"></i></div>
                    <div class="text"><strong><h3>Sport</h3></strong></div>
                  </div>
                  
                  <div class="align-items-center bg-white has shadow" style="padding: 2%;">
                      <div cass="text">
                        <hr />
                        <p><b>Hard: </b><?php echo getHardCategory('sport'); ?></p>
                        <hr />
                        <p><b>Easy: </b><?php echo getEasyCategory('sport'); ?></p>
                        <hr />
                        <p><b>Total: </b><?php echo getTotalCategory('sport'); ?></p>
                    </div>
                  </div>
                  
                </div>
                
                
                <!-- Statistics -->
                <div class="statistics col-lg-3 col-12">
                  <div class="statistic d-flex align-items-center bg-white has-shadow">
                    <div class="icon bg-yellow"><i class="fa fa-headphones"></i></div>
                    <div class="text"><strong><h3>Entertainment</h3></strong></div>
                  </div>
                  
                  <div class="align-items-center bg-white has shadow" style="padding: 2%;">
                      <div cass="text">
                        <hr />
                        <p><b>Hard: </b><?php echo getHardCategory('entertainment'); ?></p>
                        <hr />
                        <p><b>Easy: </b><?php echo getEasyCategory('entertainment'); ?></p>
                        <hr />
                        <p><b>Total: </b><?php echo getTotalCategory('entertainment'); ?></p>
                    </div>
                  </div>
                  
                </div>
                
                
                <!-- Statistics -->
                <div class="statistics col-lg-3 col-12">
                  <div class="statistic d-flex align-items-center bg-white has-shadow">
                    <div class="icon bg-green"><i class="fa fa-laptop"></i></div>
                    <div class="text"><strong><h3>Technology</h3></strong></div>
                  </div>
                  
                  <div class="align-items-center bg-white has shadow" style="padding: 2%;">
                      <div cass="text">
                        <hr />
                        <p><b>Hard: </b><?php echo getHardCategory('technology'); ?></p>
                        <hr />
                        <p><b>Easy: </b><?php echo getEasyCategory('technology'); ?></p>
                        <hr />
                        <p><b>Total: </b><?php echo getTotalCategory('technology'); ?></p>
                    </div>
                  </div>
                  
                </div>
                
                
                
                <!-- Statistics -->
                <div class="statistics col-lg-3 col-12">
                  <div class="statistic d-flex align-items-center bg-white has-shadow">
                    <div class="icon bg-blue"><i class="fa fa-users"></i></div>
                    <div class="text"><strong><h3>General</h3></strong></div>
                  </div>
                  
                  <div class="align-items-center bg-white has shadow" style="padding: 2%;">
                      <div cass="text">
                        <hr />
                        <p><b>Hard: </b><?php echo getHardCategory('general'); ?></p>
                        <hr />
                        <p><b>Easy: </b><?php echo getEasyCategory('general'); ?></p>
                        <hr />
                        <p><b>Total: </b><?php echo getTotalCategory('general'); ?></p>
                    </div>
                  </div>
                  
                </div>
                
                
                
            </div>
          </section>


                <footer class="main-footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">
                                <p>WinApay &copy; 2018-2020</p>
                            </div>
                            <div class="col-sm-6 text-right">
                                <p>Design by <a href="https://bootstrapious.com/admin-templates" class="external">Bootstrapious</a></p>
                                <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>
    <!-- Javascript files-->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="vendor/popper.js/umd/popper.min.js"> </script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="js/charts-home.js"></script>
    <!-- Main File-->
    <script src="js/front.js"></script>
</body>
</html>