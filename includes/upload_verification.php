<?php
if (isset($_POST['submit'])) {

	$category 		= sanitize($_POST['category']);
	$difficulty 	= sanitize($_POST['difficulty']);
	$mode			= sanitize($_POST['mode']);
	$question		= sanitize($_POST['question']);
	$option_a		= sanitize($_POST['option_a']);
	$option_b		= sanitize($_POST['option_b']);
	$option_c		= sanitize($_POST['option_c']);
	$option_d		= sanitize($_POST['option_d']);
	$answer			= sanitize($_POST['answer']);

	if (
		empty($category) 	||
		empty($difficulty) 	||
		empty($mode) 		||
		empty($question) 	||
		empty($option_a) 	||
		empty($option_b) 	||
		empty($option_c) 	||
		empty($option_d) 	||
		empty($answer)) 
	{
		$error = '<p style="color: #fff; background-color: #c60303; padding: 2%;">All fields are Required</p>';
	} else {
		# Check If Question already exists
		if (checkQuestionExists($question) == false) {

			# Insert Question to Database
			$query = "INSERT INTO `questions`
			(`id`, `question_name`, `answer1`, `answer2`, `answer3`, `answer4`, `answer`, `package`, `difficulty`, `category`) 
			VALUES 
			(NULL,'$question','$option_a','$option_b','$option_c','$option_d','$answer','$mode','$difficulty','$category')";

			$ins_question = mysqli_query($connection, $query);
			if ($ins_question) {
				$good = '<p style="background-color: #1c913b; color: #fff; padding: 2%;">Submitted Successfully</p>';
			}

		} else {
			$error = '<p style="color: #fff; background-color: #c60303; padding: 2%;">Question Already Exists</p>';
		}
	}

}