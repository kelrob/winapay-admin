<!-- Messages-->
                            <li class="nav-item dropdown"> 
                                <a id="messages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="badge bg-orange"><?php displayNoOfUnreadMessages(); ?></span>
                                </a>
                                
                                
                                <ul aria-labelledby="notifications" class="dropdown-menu">
                                    
                                    <?php
                                        $query = mysqli_query($connection, "SELECT * FROM `messages` WHERE `is_read` = '0' ORDER BY `id` DESC LIMIT 3");
                                        
                                        while($details = mysqli_fetch_array($query)) {
                                            $sender_id = $details['sender_id'];
                                            $sender_fullname =  getUserField($sender_id, 'fullname');
                                            $message_subject = $details['message_subject'];
                                    ?>
                                        
                                      <li>
                                        <a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                            <div class="msg-profile"> 
                                                <img src="img/avatar-1.jpg" alt="..." class="img-fluid rounded-circle">
                                            </div>
                                            <div class="msg-body">
                                              <h3 class="h5"><?php echo $sender_fullname ?></h3><span><?php echo $message_subject; ?></span>
                                            </div>
                                        </a>
                                    </li>  
                                        
                                    <?php
                                        }
                                    ?>
                                    
                                    
                                    <li>
                                        <a rel="nofollow" href="messages.php" class="dropdown-item all-notifications text-center"> 
                                            <strong>Read all messages</strong>
                                        </a>
                                    </li>
                                </ul>
                            </li>