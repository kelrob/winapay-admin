<?php
include_once 'functions/general.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>WinApay | Dashboard</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
        <!-- Fontastic Custom icon font-->
        <link rel="stylesheet" href="css/fontastic.css">
        <!-- Google fonts - Poppins -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="css/custom.css">
        <!-- Favicon-->
        <link rel="shortcut icon" href="img/favicon.png">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        <?php
            if (isset($_POST['search'])) {
                $email = sanitize($_POST['email']);
                
                if (!empty($email)) {
                    if (email_exist($email)) {
                        
                        $query_email = mysqli_query($connection, "SELECT * FROM `users` WHERE `email`='$email'");
                        
                    } else { 
                        $error = '<p style="background-color: #c60303; color: #fff; padding: 2%;">Email Does not Exists</p>';
                    }
                } else {
                    $error = '<p style="background-color: #c60303; color: #fff; padding: 2%;">All fields are reqired</p>';
                }
            }
        ?>
    </head>
<body>
    <div class="page">
        <!-- Main Navbar-->
        <header class="header">
            <nav class="navbar">

                <!-- Search Box-->
                <div class="search-box">
                    <button class="dismiss"><i class="icon-close"></i></button>
                    <form id="searchForm" action="#" role="search">
                        <input type="search" placeholder="What are you looking for..." class="form-control">
                    </form>
                </div>

                <div class="container-fluid">
                    <div class="navbar-holder d-flex align-items-center justify-content-between">

                        <!-- Navbar Header-->
                        <div class="navbar-header">
                            <!-- Navbar Brand -->
                            <a href="index.html" class="navbar-brand">
                                <div class="brand-text brand-big">
                                    <span>WinApay </span><strong>Dashboard</strong>
                                </div>
                                <div class="brand-text brand-small">
                                    <strong>W</strong>
                                </div>
                            </a>
                            <!-- Toggle Button-->
                            <a id="toggle-btn" href="#" class="menu-btn active">
                                <span></span><span></span><span></span>
                            </a>
                        </div>

                        <!-- Navbar Menu -->
                        <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">

                            <!-- Search-->
                            <li class="nav-item d-flex align-items-center">
                                <a id="search" href="#">
                                    <i class="icon-search"></i>
                                </a>
                            </li>

                            <!-- Notifications-->
                            <li class="nav-item dropdown"> 
                                <a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link">
                                    <i class="fa fa-bell-o"></i>
                                    <span class="badge bg-red">12</span>
                                </a>
                                <ul aria-labelledby="notifications" class="dropdown-menu">
                                    <li>
                                        <a rel="nofollow" href="#" class="dropdown-item"> 
                                            <div class="notification">
                                                <div class="notification-content">
                                                    <i class="fa fa-envelope bg-green"></i>You have 6 new messages 
                                                </div>
                                                <div class="notification-time">
                                                    <small>4 minutes ago</small>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a rel="nofollow" href="#" class="dropdown-item"> 
                                            <div class="notification">
                                                <div class="notification-content">
                                                    <i class="fa fa-twitter bg-blue"></i>You have 2 followers
                                                </div>
                                                <div class="notification-time">
                                                    <small>4 minutes ago</small>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a rel="nofollow" href="#" class="dropdown-item"> 
                                            <div class="notification">
                                                <div class="notification-content">
                                                    <i class="fa fa-upload bg-orange"></i>Server Rebooted
                                                </div>
                                                <div class="notification-time">
                                                    <small>4 minutes ago</small>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a rel="nofollow" href="#" class="dropdown-item"> 
                                            <div class="notification">
                                                <div class="notification-content">
                                                    <i class="fa fa-twitter bg-blue"></i>You have 2 followers
                                                </div>
                                                <div class="notification-time">
                                                    <small>10 minutes ago</small>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a rel="nofollow" href="#" class="dropdown-item all-notifications text-center"> 
                                            <strong>view all notifications</strong>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <!-- Messages-->
                            <li class="nav-item dropdown"> 
                                <a id="messages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="badge bg-orange">10</span>
                                </a>
                                <ul aria-labelledby="notifications" class="dropdown-menu">
                                    <li>
                                        <a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                            <div class="msg-profile"> 
                                                <img src="img/avatar-1.jpg" alt="..." class="img-fluid rounded-circle">
                                            </div>
                                            <div class="msg-body">
                                              <h3 class="h5">Jason Doe</h3><span>Sent You Message</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                            <div class="msg-profile"> 
                                                <img src="img/avatar-2.jpg" alt="..." class="img-fluid rounded-circle">
                                            </div>
                                            <div class="msg-body">
                                              <h3 class="h5">Frank Williams</h3><span>Sent You Message</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                                            <div class="msg-profile"> 
                                                <img src="img/avatar-3.jpg" alt="..." class="img-fluid rounded-circle">
                                            </div>
                                            <div class="msg-body">
                                              <h3 class="h5">Ashley Wood</h3><span>Sent You Message</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a rel="nofollow" href="#" class="dropdown-item all-notifications text-center"> 
                                            <strong>Read all messages</strong>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <!-- Logout    -->
                            <li class="nav-item">
                                <a href="login.html" class="nav-link logout">Logout<i class="fa fa-sign-out"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <div class="page-content d-flex align-items-stretch"> 
            <!-- Side Navbar -->
            <nav class="side-navbar">
                <!-- Sidebar Header-->
                <div class="sidebar-header d-flex align-items-center">
                    <div class="avatar">
                        <img src="img/avatar-1.jpg" alt="..." class="img-fluid rounded-circle">
                    </div>
                    <div class="title">
                        <h1 class="h4"><?php echo $adminUsername; ?></h1>
                        <p>Admin</p>
                    </div>
                </div>
                <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
                <ul class="list-unstyled">
                    <li><a href="dashboard.php"> <i class="icon-home"></i>Home </a></li>
                    <li><a href="home.php"> <i class="icon-home"></i>Ask a Question </a></li>
                    <li><a href="notify.php"> <i class="fa fa-bell-o"></i>Notify User </a></li>
                    <li><a href="cash.php"> <i class="fa fa-money"></i>Add Cash / Deduct </a></li>
                    <li><a href="request.php"> <i class="fa fa-money"></i>Withdrawal Request <span class="badge bg-red"><?php echo withdrawal_request(); ?></span> </a></li>
                    <li class="active"><a href="information.php"> <i class="fa fa-money"></i>User Information </a></li>
                </ul>
            </nav>
            <div class="content-inner">
                <!-- Page Header-->
                <header class="page-header">
                    <div class="container-fluid">
                        <h2 class="no-margin-bottom">Dashboard | <small>Get Information</small></h2>
                    </div>
                </header>

                <!-- Feeds Section-->
                <!-- Dashboard Header Section    -->
                  <section class="dashboard-header">
                    <div class="container-fluid">
                      <div class="row">
                        
                        <div class="col-lg-12">
                                <div class="card-header d-flex align-items-center">
                                    <h3 class="h4">Get Information</h3>
                                </div>

                                <div class="card">
                                    <div class="card-body">
                                        <?php 
                                            if (isset($error)) { echo $error; }
                                            if (isset($good)) { echo $good; } 
                                        ?>
                                        <form method="post" action="">
                                            <div class="form-group">
                                                <label>Enter Email</label>
                                                <input type="text" name="email" class="form-control" placeholder="Enter Email" />
                                            </div>
                                            <div class="form-group">
                                                <input type="submit" name="search" class="btn btn-primary" value="SEARCH" />
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                
                                
                                <div class="card">
                                    <div class="card-body">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>S/N</th>
                                                    <th>Account Name</th>
                                                    <th>Bank Name</th>
                                                    <th>Account Number</th>
                                                    <th>Account Type</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                        
                                                <?php 
                                                    while ($info = mysqli_fetch_array($query_email)) {
                                                        $user_id = $info['id'];
                                                        $bank_name = getBankField($user_id, 'bank_name');
                                                        $account_number = getBankField($user_id, 'account_number');
                                                        $account_name = getBankField($user_id, 'account_name');
                                                        $account_type = getBankField($user_id, 'type');
                                                ?>
                                                     <tr>
                                                         <td><?php echo $user_id; ?></td>
                                                         <td><?php echo $account_name; ?></td>
                                                         <td><?php echo $bank_name; ?></td>
                                                         <td><?php echo $account_number; ?></td>
                                                         <td><?php echo $account_type; ?></td>
                                                     </tr>   
                                                <?php
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </div>
                            
                        
                      </div>
                    </div>
                  </section>


                <footer class="main-footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">
                                <p>WinApay &copy; 2018-2020</p>
                            </div>
                            <div class="col-sm-6 text-right">
                                <p>Design by <a href="https://bootstrapious.com/admin-templates" class="external">Bootstrapious</a></p>
                                <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>
    <!-- Javascript files-->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="vendor/popper.js/umd/popper.min.js"> </script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="js/charts-home.js"></script>
    <!-- Main File-->
    <script src="js/front.js"></script>
</body>
</html>