<?php
# Session start
use Admin\Admin;
use Database\Database;
use Utils\Utils;

session_start();

# Required Class to work
require_once 'classes/Database.php';
require_once 'classes/Admin.php';
require_once 'utils/Utils.php';

# Database Objects and Properties
$db = new Database();
$conn = $db->getConnection();

# Admin Object and Properties
$admin = new Admin($conn);

# Utils Objects and Properties
$utils = new Utils($conn);

# Once the Login Button has been closed
if (isset($_POST['login'])) {

    # Entered Username and Password
    $enteredUsername = $utils->sanitize($_POST['loginUsername']);
    $enteredPassword = $utils->sanitize($_POST['loginPassword']);

# Check if email or password is empty
if (empty($enteredPassword) || empty($enteredUsername)) {
    $outputMessage = 'All Fields are Required';
} else {
    # Check if Username Exist
    if ($utils->checkValueExist('admin', 'username', $enteredUsername)) {

        $admin->username = $enteredUsername;
        $admin->password = $enteredPassword;

        # Login the Admin
        if ($admin->login()) {

            # Encrypted Password
            $encryptedPassword = password_hash($enteredPassword, PASSWORD_BCRYPT);

            # Get Old Password from the database
            $dbPassword = $utils->getTableFieldById('admin', 'password', $admin->id);
            $admin->password = $dbPassword;
            $oldPasswordCount = strlen($dbPassword);

            # Check if old Password < 20 [Change the Password]
            if ($oldPasswordCount < 20) {
                if ($utils->passMatchDbPass($enteredPassword, $dbPassword)) {
                    $utils->updateTableColumnById('admin', 'password', $encryptedPassword, $admin->id);
                    if (password_verify($enteredPassword, $encryptedPassword)) {
                        $_SESSION['admin_id'] = $admin->id;
                        header('Location: admin');
                        exit();
                    } else {
                        $outputMessage = 'Invalid ';
                    }
                } else {
                    $outputMessage = 'Invalid Login';
                }
            } else {
                if (password_verify($enteredPassword, $admin->password)) {
                    $_SESSION['admin_id'] = $admin->id;
                    header('Location: admin');
                    exit();
                } else {
                    $outputMessage = 'Invalid Login Credentials';
                }
            }
        }

    } else {
        $outputMessage = 'This Username does not Exist';
    }
}

}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>My Winapay</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
        <!-- Fontastic Custom icon font-->
        <link rel="stylesheet" href="css/fontastic.css">
        <!-- Google fonts - Poppins -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="css/custom.css">
        <!-- Favicon-->
        <link rel="shortcut icon" href="img/favicon.png">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    </head>
<body>
    <div class="page login-page">
        <div class="container d-flex align-items-center">
            <div class="form-holder has-shadow">
                <div class="row">

                    <!-- Logo & Information Panel-->
                    <div class="col-lg-6">
                        <div class="info d-flex align-items-center">
                            <div class="content">
                                <div class="logo">
                                    <h1>WinApay</h1>
                                </div>
                                <p>Staffs Login. Authorised staffs only.</p>
                            </div>
                        </div>
                    </div>

                    <!-- Form Panel    -->
                    <div class="col-lg-6 bg-white">
                        <div class="form d-flex align-items-center">
                            <div class="content">
                                <form id="login-form" method="post" action="">
                                    <?php
                                    if (isset($outputMessage)) {
                                        echo '<p style="color: #f85d51;">' . $outputMessage . '</p>';
                                    }
                                    ?>
                                    <div class="form-group">
                                        <input id="login-username" type="text" name="loginUsername" required="" class="input-material">
                                        <label for="login-username" class="label-material">User Name</label>
                                    </div>
                                    <div class="form-group">
                                        <input id="login-password" type="password" name="loginPassword" required="" class="input-material">
                                        <label for="login-password" class="label-material">Password</label>
                                    </div>
                                    
                                    <input type="submit" name="login" id="login" class="btn btn-primary" value="Login">
                                    <!-- This should be submit button but I replaced it with <a> for demo purposes-->
                                </form>
                                <!-- <a href="#" class="forgot-pass">Forgot Password?</a><br><small>Do not have an account? </small><a href="register.html" class="signup">Signup</a> -->
                             </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="copyrights text-center">
            <p>Design by <a href="https://bootstrapious.com/admin-templates" class="external">Bootstrapious</a></p>
            <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
        </div>
    </div>

    <!-- Javascript files-->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="vendor/popper.js/umd/popper.min.js"> </script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/jquery-validation/jquery.validate.min.js"></script>

    <!-- Main File-->
    <script src="js/front.js"></script>
</body>
</html>