<?php

session_start();

# Important Files that are needed
require_once '../classes/Database.php';
require_once '../classes/Admin.php';
require_once '../utils/Utils.php';

# Important Namespaces
use Admin\Admin;
use Database\Database;
use Utils\Utils;

# Database Objects and Properties
$db = new Database();
$conn = $db->getConnection();

# Admin Objects and Properties
$admin = new Admin($conn);

# Utils Objects and Properties
$utils = new Utils($conn);

# Other Important Properties
$admin->id = $utils->sanitize($_SESSION['admin_id']);
$admin->username = $utils->getTableFieldById('admin', 'username', $admin->id);

# Check if the Admin is logged In
if (!isset($_SESSION['admin_id'])) {
    header('Location: https://myquiz.winapay.com');
    exit();
}

# Determine the kind of action to take
$id =  $utils->sanitize($_GET['id']);
$stats =  $utils->sanitize($_GET['stats']);

if ($stats == 0) {
    $utils->updateTableColumnById('unattended_users', 'status', 1, $id);
    header('Location: ' . $_SERVER['HTTP_REFERER']);
    exit();

} else if ($stats == 1) {

    $userId = $utils->sanitize($_GET['uid']);
    $amount = $utils->sanitize($_GET['a']);

    $oldWallet = $utils->getTableFieldById('users', 'wallet', $userId);
    $newWallet = $oldWallet + $amount;

    $utils->updateTableColumnById('unattended_users', 'status', 1, $id);
    $utils->updateTableColumnById('users', 'wallet', $newWallet, $userId);

    header('Location: ' . $_SERVER['HTTP_REFERER']);
    exit();
}

