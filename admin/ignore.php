<?php
ob_start();
session_start();

# Important Files that are needed
require_once '../classes/Database.php';
require_once '../classes/Admin.php';
require_once '../utils/Utils.php';

# Important Namespaces
use Admin\Admin;
use Database\Database;
use Utils\Utils;

# Database Objects and Properties
$db = new Database();
$conn = $db->getConnection();

# Admin Objects and Properties
$admin = new Admin($conn);

# Utils Objects and Properties
$utils = new Utils($conn);

# Other Important Properties
$admin->id = $utils->sanitize($_SESSION['admin_id']);
$admin->username = $utils->getTableFieldById('admin', 'username', $admin->id);

if (isset($_GET['id'])) {
    $gameId = $utils->sanitize($_GET['id']);

    if ($utils->checkValueExist('users_games', 'id', $gameId) == true) {
        $utils->updateTableColumnById('users_games', 'admin_ignore', '1', $gameId);
        ?>
        <script>
            alert('Game Ignored');
        </script>
        <?php
        header('Location: questions.php');
        exit();
    }

}