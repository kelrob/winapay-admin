<?php
ob_start();
session_start();

# Important Files that are needed
require_once '../classes/Database.php';
require_once '../classes/Admin.php';
require_once '../utils/Utils.php';

# Important Namespaces
use Admin\Admin;
use Database\Database;
use Utils\Utils;

# Database Objects and Properties
$db = new Database();
$conn = $db->getConnection();

# Admin Objects and Properties
$admin = new Admin($conn);

# Utils Objects and Properties
$utils = new Utils($conn);

# Other Important Properties
$admin->id = $utils->sanitize($_SESSION['admin_id']);
$admin->username = $utils->getTableFieldById('admin', 'username', $admin->id);

if (isset($_GET['action'])) {

    $unId = $utils->sanitize($_GET['unId']);
    $action = $utils->sanitize($_GET['action']);
    $userId = $utils->sanitize($_GET['usid']);

    $oldUserPoints = $utils->getTableFieldById('users', 'current_points', $userId);
    $newUserPoints = $oldUserPoints + 100;
    
    if ($action == 'a1') {
        $utils->updateTableColumnById('proofs', 'action', 1, $unId);
        $utils->updateTableColumnById('users', 'current_points', $newUserPoints, $userId);
        header('Location: proofs.php');
        exit();
    } else if ($action == 'f9') {
        $utils->updateTableColumnById('proofs', 'action', 9, $unId);
        header('Location: proofs.php');
        exit();
    }

}