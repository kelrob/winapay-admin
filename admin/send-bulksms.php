<?php

# Important Files that are needed
require_once '../classes/Database.php';
require_once '../classes/Admin.php';
require_once '../utils/Utils.php';

# Important Namespaces
use Admin\Admin;
use Database\Database;
use Utils\Utils;

# Database Objects and Properties
$db = new Database();
$conn = $db->getConnection();

# Admin Objects and Properties
$admin = new Admin($conn);

# Utils Objects and Properties
$utils = new Utils($conn);

$campaignStatus = $utils->getTableFieldById('sms_campaign', 'status', 1);

if ($campaignStatus == 1) {

    # Start Sending the Message
    $query = $utils->getNextUserToSMS();
    while ($row = $query->fetch(PDO::FETCH_ASSOC)) {

        $userId = $row['id'];
        $fullname = $row['fullname'];
        $receiverPhone = $row['phone'];
        $message = $fullname . ', you have been selected to receive WINAPAY N20,000 Christmas bonus. Accept before December 1 at https://winapay.com?ga';

        if ($utils->sendSmsNotification($receiverPhone, $message)) {
            $utils->updateTableColumnById('users', 'bulk_sms_sent', 1, $userId);

            echo 'sent';
        }

    }

}

