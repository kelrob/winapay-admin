<?php

ob_start();
# Important Files that are needed
require_once '../classes/Database.php';
require_once '../classes/Admin.php';
require_once '../utils/Utils.php';

# Important Namespaces
use Admin\Admin;
use Database\Database;
use Utils\Utils;

# Database Objects and Properties
$db = new Database();
$conn = $db->getConnection();

# Admin Objects and Properties
$admin = new Admin($conn);

# Utils Objects and Properties
$utils = new Utils($conn);


if (isset($_GET['to'])) {
    $to = $utils->sanitize($_GET['to']);
    
    if ($to == 0) {
        $utils->updateTableColumnById('sms_campaign', 'status', 0, 1);
        die('<h1 align="center"> Turned Off <a href="bulk-sms.php">Go back</a> </h1>');
    } else if ($to == 1) {
        $utils->updateTableColumnById('sms_campaign', 'status', 1, 1);
        die('<h1 align="center"> Turned On <a href="bulk-sms.php">Go back</a> </h1>');

    }
}