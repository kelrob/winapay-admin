<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 4/16/2019
 * Time: 12:44 PM
 */

namespace Utils;


use PDO;

class Utils
{
    private $_db;

    /**
     * Calls the Database when User class is called.
     *
     * @param $conn
     */
    public function __construct($conn)
    {
        $this->_db = $conn;
    }

    /**
     * @param $string   String to Sanitize
     *
     * @return string   sanitized String
     */
    public function sanitize($string)
    {
        return htmlspecialchars(strip_tags($string));
    }

    /**
     * @param $table    String value [Table Name]
     * @param $column    String value [Column Name]
     * @param $value    String value [Value Entered]
     *
     * @return bool     Boolean Value of true or false
     */
    public function checkValueExist($table, $column, $value)
    {
        $value = addslashes($value);
        # Query that Checks if Value exist in table on the database
        $query = "SELECT COUNT(*) FROM `$table` WHERE `$column` = '$value'";

        # Perform the Direct Query
        $stmt = $this->_db->query($query);


        # Rows Returned
        $rows = (int) $stmt->fetchColumn();

        if ($rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $table        String [Table Name on the Database]
     * @param $column       String [Column Name on the Database]
     * @param $id           int [Integer Value of the table Id]
     *
     * @return mixed        Return the Value requested from the table
     */
    public function getTableFieldById($table, $column, $id) {
        $query = "SELECT `$column`
                  FROM `$table`
                  WHERE `id` = '$id'
                  LIMIT 1
                  ";
        $stmt = $this->_db->prepare($query);
        $stmt->execute();

        $tableData = $stmt->fetch();
        return $tableData[$column];
    }

    /**
     * @param $enteredPassword     String [Password Entered by the User]
     * @param $dbPassword          String [password on the Database]
     *
     * @return bool            Returns True or False
     */
    public function passMatchDbPass($enteredPassword, $dbPassword) {

        $salt           = hash('sha256',$enteredPassword);
        $password_crypt = crypt($enteredPassword, $salt);

        if ($password_crypt == $dbPassword){
            return true;
        } else {
            return false;
        }

    }

    /**
     * @param $table        String Table to Update
     * @param $column       String Column on the table to go to
     * @param $value        Integer or String Value to updated column to
     * @param $id           Integer Id to go and edit
     *
     * @return bool         Return true or false
     */

    public function updateTableColumnById($table, $column, $value, $id) {
        $value = addslashes($value);
        $query = "UPDATE $table 
                SET `$column` = '$value'
                WHERE id = '$id'";

        $stmt = $this->_db->prepare($query);

        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * @param $table    [Table to read from]
     *
     * @return mixed   [Returns an array]
     */
    public function readAll($table) {
        $query = "SELECT * FROM `$table` ORDER BY `id` DESC LIMIT 20";

        $stmt = $this->_db->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    /**
     * @return int  [Total no. of unattended users]
     */
    public function countUnattendedUsers() {
        # Query that gets the total count of the users
        $query = "SELECT COUNT(*) FROM unattended_users WHERE status = '0'";

        # Perform the Direct Query
        $stmt = $this->_db->query($query);

        # Rows Returned
        $rows = (int)$stmt->fetchColumn();

        # Return total Rows
        return $rows;
    }

    /**
     * @return int  [Total no. of unattended users]
     */
    public function countUnapprovedQuestions() {
        # Query that gets the total count of the users
        $query = "SELECT COUNT(*) FROM users_games WHERE approved = '0' AND `admin_ignore`='0'";

        # Perform the Direct Query
        $stmt = $this->_db->query($query);

        # Rows Returned
        $rows = (int)$stmt->fetchColumn();

        # Return total Rows
        return $rows;
    }

    public function countPendingProofs() {
        # Query that gets the total count of the users
        $query = "SELECT COUNT(*) FROM `proofs` WHERE `action`='0'";

        # Perform the Direct Query
        $stmt = $this->_db->query($query);

        # Rows Returned
        $rows = (int)$stmt->fetchColumn();

        # Return total Rows
        return $rows;
    }

    public function countSmsSent() {
        # Query that gets the total count of the users
        $query = "SELECT COUNT(*) FROM `users` WHERE `bulk_sms_sent`='1'";

        # Perform the Direct Query
        $stmt = $this->_db->query($query);

        # Rows Returned
        $rows = (int)$stmt->fetchColumn();

        # Return total Rows
        return $rows;
    }

    public function readAllWithCondition($table, $column1, $value1, $column2, $value2) {
        $query = "SELECT * FROM `$table`  WHERE `$column1` = '$value1' AND `$column2` = '$value2' ORDER BY  id DESC";

        $stmt = $this->_db->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    /**
     * @param $string           String value to create Permalink
     *
     * @return string|string[]|null     Return Permalink
     */
    public function createPermalink($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9-]/', '', $string); // Removes special chars.

        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }

    public function deleteFromTable($table, $column, $value) {
        $query = "DELETE FROM `$table` WHERE `$column` = '$value'";
        $this->_db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
        $stmt = $this->_db->prepare($query);

        if($result = $stmt->execute()){
            return true;
        }else {
            return false;
        }
    }

    /**
     * @param $image [Image Link]
     *
     * @return bool
     */
    public function uploadFeatureImage($image) {

        $result_message = '';

        # if image is not empty, try to upload the image
        if ($image) {

            $dir =  $_SERVER['DOCUMENT_ROOT'];
            $dir = str_replace("myquiz.winapay.com","public_html", $dir);

            $original_directory = "$dir/img/games";
            $month_year = strtolower(date('F-Y'));

            $target_directory = $original_directory . '/' . $month_year;
            $target_file = strtolower($target_directory . '/' . $image);
            $file_type = pathinfo($target_file, PATHINFO_EXTENSION);

            # error message is empty
            $errorMessage = "";

            # make sure that file is a real image
            $check = getimagesize($_FILES["image"]["tmp_name"]);
            if ($check !== false) {
                # submitted file is an image
            } else {
                $errorMessage .= "<div>Submitted file is not an image.</div>";
            }

            // make sure certain file types are allowed
            $allowed_file_types = array("jpg", "jpeg", "png", "gif");
            if (!in_array($file_type, $allowed_file_types)) {
                $errorMessage .= "<div>Only JPG, JPEG, PNG, GIF files are allowed.</div>";
            }

            // make sure file does not exist
            if (file_exists($target_file)) {
                $errorMessage .= "<div>Image already exists. Try to change file name.</div>";
            }

            // make sure submitted file is not too large, can't be larger than 1 MB
            if ($_FILES['image']['size'] > (2048000)) {
                $errorMessage .= "<div>Image must be less than 2 MB in size.</div>";
            }

            // make sure the 'uploads' folder exists
            // if not, create it
            if (!is_dir($target_directory)) {
                mkdir($target_directory, 0777, true);
            }

            // if $file_upload_error_messages is still empty
            if (empty($errorMessage)) {
                // it means there are no errors, so try to upload the file
                if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                    // it means photo was uploaded
                } else {
                    $result_message .= "<div class='alert alert-danger'>";
                    $result_message .= "<div>Unable to upload photo.</div>";
                    $result_message .= "<div>Update the record to upload photo.</div>";
                    $result_message .= "</div>";
                }
            } // if $file_upload_error_messages is NOT empty
            else {
                // it means there are some errors, so show them to user
                $result_message .= "<div class='alert alert-danger'>";
                $result_message .= "{$errorMessage}";
                $result_message .= "<div>Update the record to upload photo.</div>";
                $result_message .= "</div>";
            }

        }

        return true;
    }

    public function gameTotalPlayed($gameId) {
        # Query that gets the total count of the users
        $query = "SELECT COUNT(*) FROM game_played WHERE game_id='$gameId'";

        # Perform the Direct Query
        $stmt = $this->_db->query($query);

        # Rows Returned
        $rows = (int)$stmt->fetchColumn();

        return $rows;
    }

    public function getNextUserToSMS() {
        $query = "SELECT *
                  FROM `users`
                  WHERE `bulk_sms_sent` = '0'
                  ORDER BY id DESC 
                  LIMIT 50
                  ";
        $stmt = $this->_db->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    public function sendSmsNotification($receiverPhone, $message) {

        $prefix = '+';
        $str = $receiverPhone;

        if (substr($str, 0, strlen($prefix)) == $prefix) {
            $str = substr($str, strlen($prefix));
        }

        $url = 'http://www.mobileairtimeng.com/smsapi/bulksms.php';
        $fields = array(
            'username' => '08066781458',
            'password' => '166977e51dd11e79b3af2d',
            'message' => $message,
            'mobile' => $str,
            'sender' => 'WinApay',
            'normal route' => 1,
            'corporate route (Delivery to DND)' => 2,
            'vtype' => 1
        );

        //url-ify the data for the POST
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string, '&');

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

        //execute post
        $result = curl_exec($ch);


        //close connection
        curl_close($ch);

        return true;
    }

}